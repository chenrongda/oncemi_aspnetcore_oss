### 项目删除通知

Gitee开源项目将全部迁移至Github，Gitee仓库全部废弃。如有需要，请前往项目[Github主页](https://github.com/oncemi "Github")。  
  
我本将心向明月，奈何明月照沟渠。